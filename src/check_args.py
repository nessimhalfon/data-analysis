#
# check_args.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

from pathlib import Path
from my_bool import *

class CheckArgs:
    """
    Cette classe à pour objectif la verification des arguments passes en parametres.
    """

    def __init__(self):
        """
        Constructeur de la classe.
        """

        # Creation d'un objet ArgumentParser.
        parser = argparse.ArgumentParser()
        # Ajout de l'argument path contenant le chemin du fichier à traiter.
        parser.add_argument("path", help="Le chemin du fichier de data")
        # Ajout de l'argument dialog permettant d'activer ou non le dialogue entre l'utilisateur et le programme.
        parser.add_argument("dialog", type=my_bool, help=" Choix pour Dialogue -> (oui, o, yes, y, vrai, v, true, t, 1) | Pas de Dialogue -> (non, no, n, faux, false, f, 0)")
        # Ajout de l'argument save permettant à l'utilisateur de sauvegarder ou non les donnees traitees.
        parser.add_argument("save", type=my_bool, help="Choix pour Sauvegarde -> (oui, o, yes, y, vrai, v, true, t, 1) | Pas de Sauvegarde -> (non, no, n, faux, false, f, 0)")
        args = parser.parse_args()

        # Affectation des attributs de la classe.
        self.path_file = Path(args.path)
        self.is_dialog = args.dialog
        self.is_save = args.save

    def check_file(self):
        """
        Cette méthode permet la vérification du fichier à traité.
        """
        # Tentative d'ouverture du fichier.
        try:
            with open(self.path_file, 'r', encoding='utf-8'):
                pass
        # Echec si fichier non trouve.
        except FileNotFoundError:
            print("Erreur! Le fichier n\'a pas été trouvé")
            quit()
        # Echec si erreur lors de l'ouverture du fichier.
        except FileExistsError:
            print("Erreur! Le fichier n\'a pas pu etre ouvert")
            quit()

    def get_is_dialog(self):
        """
        Cette methode permet de retourner l'argument dialog.
        """
        return self.is_dialog

    def get_is_save(self):
        """
        Cette methode permet de retourner l'argument save.
        """
        return self.is_save

    def get_path_file(self):
        """
        Cette methode permet de retourner l'argument file.
        """
        return self.path_file
