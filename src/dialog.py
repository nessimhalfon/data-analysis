#
# dialog.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

class Dialog:
    """
    Cette classe permet le dialogue entre l'utilisateur et le programme.
    """

    def __init__(self, display):
        """
        Constructeur de la classe.
        """

        # L'affichage des donnees.
        self.display = display
        # L'affichage des informations du fichier.
        self.file_infos = self.display.file_infos_to_string()
        # L'affichage des informations de la DataFrame.
        self.df_infos = self.display.df_infos_to_string()
        # L'affichage des informations des variables qualitatives.
        self.quali_infos = self.display.quali_infos_to_string()
        # L'affichage des informations des variables quantitatives.
        self.quanti_infos = self.display.quanti_infos_to_string()

    def with_dialog(self):
        """
        Cette methode permet de dialoguer avec l'utilisateur.
        """

        # Variable contenant le choix sur l'affichage des informations du fichier.
        ask_file_infos = input("Voulez-vous afficher les informations du fichier ? (o/n) ")
        # Permet de re demander lorsque la reponse n'est pas correcte.
        while ask_file_infos != 'o' and ask_file_infos != 'n':
            ask_file_infos = input("Voulez-vous afficher les informations du fichier ? (o/n) ")
        if ask_file_infos == 'o':
            # Affichage des informations du fichier.
            print(self.file_infos)

        # Affichage des informations de la DataFrame.
        print(self.df_infos)

        # Variable contenant le choix sur l'affichage des informations des variables qualitatives.
        ask_quali_infos = input("Voulez-vous afficher les informations détaillées sur les variables qualitatives ? (o/n) ")
        # Permet de re demander lorsque la reponse n'est pas correcte.
        while ask_quali_infos != 'o' and ask_quali_infos != 'n':
            ask_quali_infos = input("Voulez-vous afficher les informations détaillées sur les variables qualitatives ? (o/n) ")
        if ask_quali_infos == 'o':
            # Affichage des informations des variables qualitatives.
            print(self.quali_infos)

        # Variable contenant le choix sur l'affichage des informations des variables quantitatives.
        ask_quanti_infos = input("Voulez-vous afficher les informations détaillées sur les variables quantitatives ? (o/n) ")
        # Permet de re demander lorsque la reponse n'est pas correcte.
        while ask_quanti_infos != 'o' and ask_quanti_infos != 'n':
            ask_quanti_infos = input("Voulez-vous afficher les informations détaillées sur les variables quantitatives ? (o/n) ")
        if ask_quanti_infos == 'o':
            # Affichage des informations des variables quantitatives.
            print(self.quanti_infos)
