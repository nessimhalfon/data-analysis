#
# save_file.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

class SaveFile:
    """
    Cette classe permet de sauvegarder les differentes informations traitees dans un fichier txt.
    """

    def __init__(self, data_to_save):
        """
        Constructeur de la classe.
        """
        self.data_to_save = data_to_save

    def simple_save(self):
        """
        Cette methode permet de sauvegarder le fichier dans le repertoire courant.
        """
        # Permet d'ouvrir un fichier en mode ecriture dans le repertoire courant.
        file = open("log.txt", "w", encoding='utf-8')
        # Ecriture des donnees traitees dans le fichier.
        file.write(self.data_to_save)
        # Fermeture du fichier.
        file.close()
        print("Votre fichier a bien été enregistré dans le répertoire courant.\n")

    def save_with_path(self):
        """
        Cette methode permet de sauvegarder le fichier dans un repertoire a l'emplacement fournit par l'utilisateur.
        """

        # Permet de redemander a l'utilisateur si le chemin saisi n'est pas correct.
        while True:
            # Variable contenant l'emplacement dans lequel sera creer le fichier.
            path = input("\nVeuillez indiquer le chemin du répertoire dans lequel vous voulez enregistrer les données :\n")
            # Tentative d'ouverture du fichier en mode ecriture.
            try:
                # Permet d'ouvrir un fichier en mode ecriture a l'emplacement saisi par l'utilisateur.
                file = open(path + "//" + "log.txt", "w", encoding='utf-8')
                # Ecriture des donnees traitees dans le fichier.
                file.write(self.data_to_save)
                # Fermeture du fichier.
                file.close()
                print("Votre fichier a bien été sauvegardé à cet emplacement :", path, "\n")
            # Echec si le chemin n'est pas trouve.
            except FileNotFoundError:
                print("Le chemin saisi ne correspond à aucun emplacement existant.\n")
            else:
                break
